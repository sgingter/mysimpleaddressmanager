# MySimpleAddressManager

My simple address manager is a demo application for working with AngularJS.

This file should document my progress and log my learnings.

# Dev steps

1. Add Bootstrap to the project  
I don't want to care about GUI, so I use bootstrap for this.
2. Do NOT add jQuery to the project  
Bootstrap says it requires jQuery to function properly. Nevertheless, I want to work with AngularJS. And there is a module for Angular that enables you to use Boostrap with Angular without the need for jQuery.
3. Add AngularJS to the project  
